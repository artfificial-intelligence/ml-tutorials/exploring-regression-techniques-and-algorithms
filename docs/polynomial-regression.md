# 다항식 회귀

## 다항식 회귀 기법

다항식 회귀는 선형 회귀의 일반화로서 종속 변수와 독립 변수 사이의 비선형 관계를 허용한다. 오차 제곱의 합을 최소화하는 가장 적합한 다항식 곡선을 찾으려고 한다.

다항 회귀 모형의 방정식은 다음과 같다.

$y = \beta_0 + \beta_1x + \beta_2 x^2 + ... + \beta_n x^n + \epsilon$

여기서 y는 종속 변수, x는 독립 변수, $\beta0$, $\beta_1$, …, $\beta_n$은 계수, n은 다항식의 차수, $\epsilon$은 오차항이다.

계수들은 다항식의 가중치를 나타내고, 오차항은 예측값으로부터 실제값의 편차를 나타낸다. 다항식 회귀의 목표는 선형 회귀와 동일하게 오차 제곱합을 최소화하는 계수들의 최적값을 찾는 것이다.

그러나 다항식 회귀분석은 선형회귀분석과 달리 모형에 고차항을 추가함으로써 비선형 관계를 다룰 수 있다. 다항식의 정도에 따라 모형의 복잡성과 유연성이 결정된다. 정도가 높으면 데이터에 더 잘 맞을 수 있지만 과적합과 높은 분산을 유발할 수도 있다. 정도가 낮으면 과적합을 방지할 수 있지만 과소적합과 높은 편향을 유발할 수도 있다. 따라서 다항식의 최적화 정도를 선택하는 것은 편향과 분산의 상충관계이며 데이터와 문제에 따라 달라진다.

파이썬을 이용하여 다항식 회귀를 구현하기 위해서는 앞 절에서 이미 학습한 Scikit-learn 라이브러리를 사용할 것이다. Scikit-learn은 원래의 특징으로부터 다항식과 상호작용 특징을 생성할 수 있는 다항식 특징이라는 함수를 제공한다. 또한 앞 절에서 이미 학습한 NumPy와 Pandas 라이브러리를 사용할 것이다. 

## Python에서 다항식 회귀 <sup>[1](#footnote_1)</sup>
다항식 회귀를 수행하기 위해 먼저 데이터를 플롯하고 분석하여 가장 적합한 다항식을 결정한다. 그런 다음 다항식 회귀 모델은 종속 변수의 관측값과 예측값의 차이를 최소화하도록 다항식 항의 계수를 조정하여 훈련된다. 결과 방정식을 사용하여 새로운 데이터에 대한 예측을 수행할 수 있다.

이 예에서 직원의 직급과 급여에 관한 [position salary 데이터](https://www.kaggle.com/datasets/akram24/position-salaries)를 사용할 것이다. 이 데이터세트에는 `Position`, `Level` 및 `Salary`의 세 열이 있다.

### 필요한 Python 패키지 임포트 <sup>[2](#footnote_2)</sup>
데이터 조작을 위한 Pandas, 수학적 계산을 위한 NumPy, 시각화를 위한 MatplotLib와 Seaborn이 필요하다. Sklearn 라이브러리는 기계 학습 작업에 사용된다.

```python
# Import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
```

### 데이터세트 로드
여기<sup>[3](#footnote_3)</sup>에서 데이터세트를 다운로드하여 Notebook에 업로드하고 Pandas 데이터 프레임으로 읽어드린다.

```python
# Get dataset
df_sal = pd.read_csv('/content/Position_Salaries.csv')
df_sal.head()
```

![](./images/screenShot_41.png)

### 데이터 분석
이제 데이터가 준비되었으니, 데이터의 경향을 자세히 분석하고 이해해 보도록 하자. 그러기 위해서 먼저 아래의 데이터로 설명할 수 있다.

```python
# Describe data
df_sal.describe()
```

![](./images/screenShot_42.png)

여기에서 급여 범위는 45,000에서 1,000,000이며 중앙값은 130,000입니다.

또한 Seaborn distplot을 사용하여 데이터의 시각적 분포를 볼 수 있다.

```python
# Data distribution
plt.title('Salary Distribution Plot')
sns.distplot(df_sal['Salary'])
plt.show()
```

![](./images/screenShot_43.png)

산점도 또는 분포도는 데이터 분포의 변화를 보여준다.
히스토그램에 선을 결합하여 데이터를 나타낸다.

그런 다음 `Salary`와 `Level`의 관계를 확인한다

```python
# Relationship between Salary and Level
plt.scatter(df_sal['Level'], df_sal['Salary'], color = 'lightcoral')
plt.title('Salary vs Level')
plt.xlabel('Level')
plt.ylabel('Salary')
plt.box(False)
plt.show()
```

![](./images/screenShot_44.png)

이제 분명히 알 수 있다, 데이터는 다항식 포물선 곡선처럼 변한다. 그것은 개인의 급여가 그들의 수준이 증가함에 따라 기하급수적으로 증가한다는 것을 의미한다.

### 데이터 집합을 종속 변수/독립 변수로 분할
`Experience(X)`는 독립 변수며, `Salary(y)`는 경력에 의존한다.

```python
# Splitting variables
X = df_sal.iloc[:, 1:-1].values  # independent
y = df_sal.iloc[:, -1].values  # dependent
```

### 휘귀 모델 훈련

```python
# Train linear regression model on whole dataset
lr = LinearRegression()
lr.fit(X, y)

# Train polynomial regression model on the whole dataset
pr = PolynomialFeatures(degree = 4)
X_poly = pr.fit_transform(X)
lr_2 = LinearRegression()
lr_2.fit(X_poly, y)
```

### 결과 예측
여기서 흥미로운 부분이 있다. 우리가 `regressor.predictor`를 사용하는 훈련된 모델은 `X`(`Position`, `Level`)에 의존하는 `y`(`Salary`)의 값을 예측할 준비가 되어 있다.

```python
# Predict results
y_pred_lr = lr.predict(X)           # Linear Regression
y_pred_poly = lr_2.predict(X_poly)  # Polynomial Regression
```

## 예측 시각화
그래프를 표시하여 예측 결과를 테스트할 것이다.

- 선형 회귀를 사용한 예측

    먼저 회귀선에 대한 선형 회귀를 통해 실제 데이터와 예측값 사이의 그래프를 표시한다.

```python
# Visualize real data with linear regression
plt.scatter(X, y, color = 'lightcoral')
plt.plot(X, lr.predict(X), color = 'firebrick')
plt.title('Real data (Linear Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.legend(['X/y_pred_lr', 'X/y'], title = 'Salary/Level', loc='best', facecolor='white')
plt.box(False)
plt.show()
```

![](./images/screenShot_45.png)

- 다항식 회귀를 이용한 예측

    그런 다음 다항식 회귀 분석에 의해 예측된 값으로 동일한 데이터를 확인하고 실제 데이터 점에 정확하게 닿는 곡선을 얻는다.

```python
# Visualize real data with polynomial regression
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'lightcoral')
plt.plot(X, lr_2.predict(X_poly), color = 'firebrick')
plt.title('Real data (Polynomial Regression)')
plt.xlabel('Position Level')
plt.ylabel('Salary')
plt.legend(['X/y_pred_poly', 'X/y'], title = 'Salary/Level', loc='best', facecolor='white')
plt.box(False)
plt.show()
```

![](./images/screenShot_46.png)

## 예
`Level 7.5`의 예를 들어 모델들이 예측하는 급여가 무엇인지, 얼마나 정확한지 확인해 보자.


```python
Predict a new result with linear regression
print(f'Linear Regression result : {lr.predict([[7.5]])}')

# Predict a new result with polynomial regression
print(f'Polynomial Regression result : {lr_2.predict(pr.fit_transform([[7.5]]))}')
```

```
Linear Regression result : [411257.57575758]
Polynomial Regression result : [225126.29297787]
```

이제 지수 데이터의 경우 다항식 회귀 모델이 더 높은 정확도로 결과를 예측할 수 있다는 것이 분명하다.

## 맺으며
결론적으로 다항 회귀 분석은 변수들 간의 복잡한 관계를 모형화하는 데 유용한 도구이다. 특정 유형의 자료에 대해 선형 회귀 분석보다 더 정확한 예측을 제공할 수 있다.

다항식 회귀의 가장 큰 장점 중 하나는 변수들 간의 비선형 관계를 맞출 수 있고, 이는 데이터 내의 기본 관계를 보다 정확하게 표현할 수 있다는 것이다. 그러나 고차 다항식은 과적합을 초래할 수도 있는데, 여기서 모델은 기본 관계 대신 데이터 내의 잡음에 맞출 수 있다. 이로 인해 새로운 데이터에 대한 예측이 원활 하지 않을 수 있다. 이 문제를 완화하기 위해 다항식에 적절한 차수를 선택하기 위해 교차 검증, 정규화, 모델 선택과 같은 기술을 사용할 수 있다.

<a name="footnote_1">1</a>: [Polynomial Regression in Python](https://medium.com/@shuv.sdr/polynomial-regression-in-python-58198fb0973f)의 예를 참조하였다.

<a name="footnote_2">2</a>: [Polynomial Regression](https://github.com/shuv50/Data-Science/blob/main/Polynomial_Regression.ipynb)에서 코드를 확인할 수 있다.

<a name="footnote_3">3</a>: [Polynomial_Position_salary_data](https://www.kaggle.com/datasets/testpython/polynomial-position-salary-data)에서 데이터세트를 다운로드할 수 있다.
