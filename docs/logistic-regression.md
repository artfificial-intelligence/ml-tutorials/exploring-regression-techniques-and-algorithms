# 로지스틱 회귀

## 로지스틱 회귀 기법
로지스틱 회귀 분석은 이항 분류 문제에 사용되는 특수한 유형의 회귀 분석 기법이다. 종속변수가 0 또는 1, 예 또는 아니오 등 두 가지 값만 취할 수 있는 범주형 변수라고 가정한다. 자료의 가능성을 극대화하는 가장 적합한 로지스틱 곡선을 찾고자 한다.

로지스틱 회귀 모형의 방정식은 다음과 같다.

$$x = {1 \over {1 + e^{-({\beta_0 + \beta_1 x_1 + \beta_2 x_2 + ... + \beta_n x_n})}}}$$

계수는 종속 변수의 log-odds가 1임을 나타내며, 로지스틱 함수는 log-odds를 0과 1 사이의 확률로 매핑한다. 로지스틱 회귀 분석의 목표는 관측된 결과의 확률 곱인 우도(likelihood) 함수를 최대화하는 계수의 최적값을 찾는 것이다.

계수를 추정하는 방법에는 MLE(maximum likelihood estimation) 방법, 경사 하강법, 뉴턴-랩슨(Newton-Rapson) 방법 등 다양한 방법이 있다. 이 포스팅에서는 가장 일반적이고 간단한 방법인 MLE 방법을 사용할 것이다. MLE 방법은 우도 함수가 최대값에 도달할 때까지 계수를 반복적으로 업데이트하여 계산하며, 다음과 같이 쓸 수 있다.

$\beta^{(t+1)} = {\beta}^{(t)} + \alpha \mathbf{X}^T{(\mathbf{y} - \mathbf{\hat{y}})}$

여기서 $\mathbf{\beta}^{(\mathbf{t})}$는 반복 $t$에서 계수 벡터, $\alpha$는 학습률, $\mathbf{X}$는 독립 변수의 행렬, $\mathbf{y}$는 실제 결과의 벡터, $\mathbf{\hat{y}}$는 예측 결과의 벡터이다.

Python을 이용하여 로지스틱 회귀를 구현하기 위해서는 기계 학습에 인기 있는 라이브러리인 Scikit-learn 라이브러리를 활용할 것이다. Scikit-learn은 로지스틱 회귀분석과 같이 기계 학습 모델을 구축, 훈련, 평가하기 위한 다양한 함수과 메서드을 제공한다. [이전 예](./linear-regression.md)에서 이미 학습한 NumPy와 Pandas 라이브러리도 활용할 것이다.

## 로지스틱 회귀 분석을 사용한 이항 분류 <sup>[1](#footnote_1)</sup>
이항 분류를 보이는 데 사용되는 대표적인 공개 데이터 세트 중 하나는 1912년 4월 15일 RMS 타이타닉호가 침몰했을 때 탑승한 승객 중 891명을 나열한 [Titanic 데이터세트](https://www.kaggle.com/c/titanic)이다. 데이터세트에는 각 승객의 이름뿐만 아니라 요금 등급, 요금 가격, 사람의 나이와 성별, 그리고 그 사람이 침몰에서 살아남았는지 등의 다른 정보가 포함되어 있다. 이 예에서는 승객의 생존 여부를 예측하는 이항 분류 모델을 구축할 것이다. 학습 알고리즘으로 로지스틱 회귀 분석을 사용한다.

### 데이터 세트를 로드와 준비
첫 번째 단계는 데이터세트를 로드하고 기계 학습 모델을 훈련하기 위해 준비하는 것이다. Titanic 데이터세트가 인기 있는 이유 중 하나는 데이터 과학자들이 데이터 정제(data-cleaning) 기술을 연습할 수 있는 충분한 기회를 제공하기 때문이다.

```python
import pandas as pd

df = pd.read_csv('/content/titanic.csv')
df.head()
```

![](./images/screenShot_31.png)

데이터세트의 열 중에 결측값이 있는지 여부를 확인한다.

```python
df.info()
```

![](./images/screenShot_32.png)

데이터세트를 필터링하여 사용되지 않는 열을 제거하고 "`Sex`" 와 "`Pclass`" 열을 원 핫 인코딩(one-hot-encode)하고 결측값이 있는 행을 제거한다.

```python
df = df[['Survived', 'Age', 'Sex', 'Pclass']]
df = pd.get_dummies(df, columns=['Sex', 'Pclass'])
df.dropna(inplace=True)
df.head()
```

![](./images/screenShot_33.png)

다음 단계는 데이터를 훈련용 데이터 세트와 테스트용 데이터 세트 두 개로 분할하는 것이다. 계층화 분할을 사용하여 훈련용 데이터 세트와 테스트용 데이터 세트에서 샘플의 균형 잡힌 분포를 만들 것이다.

```python
from sklearn.model_selection import train_test_split

x = df.drop('Survived', axis=1)
y = df['Survived']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, stratify=y, random_state=0)
```

### 모델 구축과 훈련
우리의 분류기는 로지스틱 회귀를 사용할 것이다. 로지스틱 회귀는 예측을 할 뿐만 아니라 확률도 산출한다.

```python
from sklearn.linear_model import LogisticRegression

model = LogisticRegression(random_state=0)
model.fit(x_train, y_train)
```

```
LogisticRegression(random_state=0)
```

정확도 얻을 수 있다.

```python
model.score(x_test, y_test)
```

```
0.8321678321678322
```

5 folds를 사용하여 모델을 교차 검증한다.

```python
from sklearn.model_selection import cross_val_score

cross_val_score(model, x, y, cv=5).mean()
```

```
0.7857480547621394
```

혼동 행렬(confusion matrix)을 사용하여 검정하는 동안 모델이 어떻게 수행되었는지 확인할 수 있다.

```python
from sklearn.metrics import confusion_matrix

y_predicted = model.predict(x_test)
cm = confusion_matrix(y_test, y_predicted, labels=model.classes_)
cm
```

```
array([[78,  7],
       [17, 41]])
```

혼동 행렬을 다시 인쇄한다. 그러나 이번에는 명확성을 위해 `plot_confusion_matrix`를 사용한다.

```python
%matplotlib inline
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay

disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                              display_labels=model.classes_)
disp.plot()
```

![](./images/screenShot_34.png)

정밀도, 리콜 및 기타 메트릭을 보려면 분류 보고서를 인쇄하세요.

```python
from sklearn.metrics import classification_report

print(classification_report(y_test, y_predicted))
```

```
              precision    recall  f1-score   support

           0       0.82      0.92      0.87        85
           1       0.85      0.71      0.77        58

    accuracy                           0.83       143
   macro avg       0.84      0.81      0.82       143
weighted avg       0.83      0.83      0.83       143
```

마지막으로 ROC 곡선을 그려 모델의 정확도를 시각화한다.

```python
from sklearn.metrics import RocCurveDisplay

disp = RocCurveDisplay.from_estimator(model, x_test, y_test)
```

![](./images/screenShot_35.png)

### 모델을 사용하여 예측 수행
이제 모델을 이용하여 일등석을 타고 여행하는 30세 여성이 항해에서 살아남을 것인지 예측해 보자.

```python
female = pd.DataFrame([{'Age':30, 'Sex_female':1, 'Sex_male':0, 'Pclass_1':1, 'Pclass_2':0,	'Pclass_3':0}])
model.predict(female)[0]
```

```
1
```

더 중요한 것은, 일등석을 타고 여행하는 30세 여성이 살아남을 확률은 얼마인가?

```python
probability = model.predict_proba(female)[0][1]
print(f'Probability of survival: {probability:.1%}')
```

```
Probability of survival: 91.6%
```

60세 남성이 3등석으로 여행할 때는 어떨까?

```python
male = pd.DataFrame([{'Age':60, 'Sex_female':0, 'Sex_male':1, 'Pclass_1':0, 'Pclass_2':0,	'Pclass_3':1}])
probability = model.predict_proba(male)[0][1]
print(f'Probability of survival: {probability:.1%}')
```

```
Probability of survival: 2.9%
```


<a name="footnote_1">1</a>: [Binary classification using logistic regression](https://github.com/jeffprosise/Machine-Learning/blob/master/Titanic%20(Logistic%20Regression).ipynb)의 예를 참조하였다.
