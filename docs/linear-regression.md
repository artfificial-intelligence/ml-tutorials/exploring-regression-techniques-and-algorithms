# 선형 회귀

## 선형 회귀 기법
선형 회귀는 가장 간단하고 널리 사용되는 회귀 기법 중 하나이다. 종속 변수와 독립 변수 간의 관계가 선형이라고 가정하고 오차 제곱의 합(sum of squared errors)을 최소화하는 가장 적합한 직선을 찾고자 한다.

선형 회귀 모델의 방정식은 다음과 같다.

$y =  \beta_0 + \beta_1x_1 + \beta_2x_2 + ... + \beta_nx_n + \epsilon$

여기서 $y$는 종속 변수, $x_1$, $x_2$, …, $x_n$은 독립 변수, $\beta_0$, $\beta_1$, …, $\beta_n$은 계수, $\epsilon$은 오차항이다.

계수들은 선의 기울기를 나타내고, 오차항은 예측값으로부터 실제값의 편차를 나타낸다. 선형 회귀의 목표는 오차 제곱합을 최소화하는 계수들의 최적값을 찾는 것으로, 이를 비용 함수 또는 손실 함수라고도 한다.

계수를 추정하는 방법에는 통상 최소 자승(OLS: ordinary least squares)법, 경사 하강(gradient descent)법, 정규 방정식(normal equation)법 등 다양한 방법이 있다. 이 포스팅에서는 가장 일반적이고 간단한 방법인 OLS 방법을 사용할 것이다. OLS 방법은 일차 방정식의 체계를 풀어서 계수를 계산하는데, 다음과 같이 쓸 수 있다.

$\mathbf{X}^T \mathbf{X} \mathbf{\beta} = \mathbf{X}^T\mathbf{y}$


여기서 $\mathbf{X}$는 독립 변수의 행렬, $\mathbf{y}$는 종속 변수의 벡터, $\mathbf{\beta}$는 계수의 벡터이다. 계수에 대한 해는 다음과 같다.

$\mathbf{\beta} = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T\mathbf{y}$

Python을 이용하여 선형 회귀를 구현하기 위해서는 과학 컴퓨팅에 널리 사용되는 라이브러리인 NumPy 라이브러리를 이용할 것이다. NumPy는 배열과 행렬을 만들고, 조작하고, 연산을 수행하는 등의 다양한 기능과 방법을 제공한다. 데이터 분석에 널리 사용되는 라이브러리인 Pandas 라이브러리도 이용할 것이다. Pandas는 데이터를 읽고, 쓰고, 처리하는 등의 데이터 구조와 형식으로 작업하기 위한 다양한 기능과 방법을 제공한다.

<!-- 
[다음 절](#sec_05)에서는 Python을 사용하여 샘플 데이터세트에 대해 선형 회귀를 수행하는 방법의 예를 볼 것이다.

example: https://www.knowledgehut.com/blog/data-science/linear-regression-for-machine-learning
https://www.freecodecamp.org/news/build-a-linear-regression-model-with-an-example/

dataset "car_data.csv" downloadable from https://github.com/5minslearn/Car-Price-Prediction-using-Linear-Regression/blob/master/car%20data.csv
-->

## 선형 회귀 예 – 중고 자동차 가격 예측 모델 <sup>[1](#footnote_1)</sup>
중고차 데이터세트는 자동차의 이름, 연도, 판매 가격, 현재 가격, 주행한 거리, 연료 종류, 판매자 종류, 변속기, 판매자가 소유자인지 등을 담고 있다. 목표는 자동차의 판매 가격을 예측하는 것이다.

해결해 보도록 하자.

### 필요한 패키지 임포트
이 문제를 해결하려면 다양한 패키지가 필요합니다. 모두 가져올 수 있는 방법은 다음과 같다.

```python
# Import the required packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.model_selection import KFold
from sklearn.pipeline import make_pipeline
from statsmodels.stats.diagnostic import normal_ad
from statsmodels.stats.outliers_influence import variance_inflation_factor
from statsmodels.stats.stattools import durbin_watson
from scipy import stats
```

### 데이터세트 임포트
데이터세트(car data.csv)는 [Kaggle](https://www.kaggle.com/datasets/nehalbirla/vehicle-dataset-from-cardekho)에서 다운로드하거나 [Github repo](https://github.com/5minslearn/Car-Price-Prediction-using-Linear-Regression/blob/master/car%20data.csv)에서 다운로드할 수 있다.

```python
# Import the dataset
df = pd.read_csv('/content/car_data.csv')
```

### 데이터세트 전처리
아래 코드는 열과 데이터 타입 및 행 수를 보여준다. 우리의 데이터세트는 9개의 열과 301개의 행으로 구성되어 있다.

```python
# rows, etc
df.info()
```

![](./images/screenShot_01.png)

"Car_Name" 열은 자동차 이름을 설명한다. 이 필드는 데이터세트에서 무시해야 한다. 자동차의 이름이 아니라 자동차의 특징만 중요하기 때문이다.

아래 코드는 데이터세트에서 고유한 자동차 이름의 수를 반환한다.

```python
# Find number of unique entries in the "Car_Name" column
df['Car_Name'].nunique()
```

데이터세트에는 98개의 고유한 자동차 이름이 있다.

범주가 너무 많기 때문에 데이터세트에 의미를 부여하지 않는 것은 분명하다. 그 열을 제거하겠다.

```python
# Remove the Car_Name column
df.drop('Car_Name', axis=1, inplace=True)
```

데이터세트에는 "`Year`"라는 열이 있다. 이상적으로는 구입/판매한 연도 이상의 자동차 age가 필요하다. 따라서 이 열을 "`Age`"으로 변환하고 "`Year`" 열을 제거한다.

```python
# Remove the Year column and replace Year with Age of the car
df.insert(0, "Age", df["Year"].max()+1-df["Year"] )
df.drop('Year', axis=1, inplace=True)
```

"`Age`"는 데이터세트에서 사용할 수 있는 최대 연도와 해당 자동차의 연도의 차이를 구함으로써 계산된다. 왜냐하면 계산은 특정 기간과 이 데이터세트에만 적용되기 때문이다.

![](./images/screenShot_02.png)

#### 이상값을 찾는 방법
이상값은 다른 관측치들과 현저하게 다른 자료점이다. 이들은 모형의 성능을 떨어뜨릴 수 있다.

![](./images/image-26.png)

범주형 열은 데이터 타입이 "`object`"일 것이다. NumPy 배열에서 숫자형 열과 범주형 열을 그룹화하자. 배열에서 처음 5개의 요소는 숫자형 열이 되고 나머지 3개는 범주형 열이 되도록 한다.

`seaborn` 라이브러리를 사용하여 열의 데이터를 플롯할 수 있다. 범주형 열은 여러 개의 막대를 디스플레이하는 반면, 수치형 열은 단일 막대를 디스플레이한다.

다음 코드를 사용하여 데이터 집합에서 이상치를 찾아보자.

```python
# Find the outliers in each column and draw them as graph
sns.set_style('darkgrid')
colors = ['#0055ff', '#ff7000', '#23bf00']
CustomPalette = sns.set_palette(sns.color_palette(colors))

OrderedCols = np.concatenate([df.select_dtypes(exclude='object').columns.values,  df.select_dtypes(include='object').columns.values])

fig, ax = plt.subplots(2, 4, figsize=(15,7),dpi=100)

for i,col in enumerate(OrderedCols):
    x = i//4
    y = i%4
    if i<5:
        sns.boxplot(data=df, y=col, ax=ax[x,y])
        ax[x,y].yaxis.label.set_size(15)
    else:
        sns.boxplot(data=df, x=col, y='Selling_Price', ax=ax[x,y])
        ax[x,y].xaxis.label.set_size(15)
        ax[x,y].yaxis.label.set_size(15)

plt.tight_layout()    
plt.show()
```

![](./images/screenShot_03.png)

**사분위간 범위 규칙(InterQuartile Range rule)**을 사용하여 이상치를 찾도록 한다.

이는 데이터세트을 4등분하는 사분위수 개념에 기반을 두고 있다. IQR(Inter Quartile Range rule) 규칙은 특히 데이터의 중간 50% 이내의 값의 범위에 초점을 맞추고 이 범위를 사용하여 잠재적인 이상치를 식별한다.

범주형 열에서 각 고유값에 대한 최소 및 최대 분위값을 구하고 목표 열의 25번째 및 75번째 백분위수에 맞지 않는 이상치 표본을 필터링해야 한다.

반면에 숫자열의 이상치들은 같은 열의 25번째와 75번째 백분위수에 의해 걸러질 수 있다. 우리는 대상 열에 대해 걸러낼 필요가 없다.

```python
# show the indices of the detected outliers
outliers_indexes = []
target = 'Selling_Price'

for col in df.select_dtypes(include='object').columns:
    for cat in df[col].unique():
        df1 = df[df[col] == cat]
        q1 = df1[target].quantile(0.25)
        q3 = df1[target].quantile(0.75)
        iqr = q3-q1
        maximum = q3 + (1.5 * iqr)
        minimum = q1 - (1.5 * iqr)
        outlier_samples = df1[(df1[target] < minimum) | (df1[target] > maximum)]
        outliers_indexes.extend(outlier_samples.index.tolist())
        
        
for col in df.select_dtypes(exclude='object').columns:
    q1 = df[col].quantile(0.25)
    q3 = df[col].quantile(0.75)
    iqr = q3-q1
    maximum = q3 + (1.5 * iqr)
    minimum = q1 - (1.5 * iqr)
    outlier_samples = df[(df[col] < minimum) | (df[col] > maximum)]
    outliers_indexes.extend(outlier_samples.index.tolist())
    
outliers_indexes = list(set(outliers_indexes))
print('{} outliers were identified, whose indices are:\n\n{}'.format(len(outliers_indexes), outliers_indexes))
```

위의 코드를 실행하여 데이터세트에 38개의 이상치가 있음을 발견했다.

![](./images/screenShot_04.png)

그러나 이상치를 제거하는 것이 항상 올바른 결정은 아니라는 것을 명심해야 한다. 이상치 제거를 결정하기 전에 이상치의 특성을 조사하는 것이 중요하다.

이상치는 다음 두 가지 경우에 삭제할 수 있다.

1. 이상치는 잘못 입력되거나 잘못 측정된 데이터로 인한 것이다
2. 이상치는 유의한 연관성을 생성한다

더 파고들어 완벽한 이상치를 찾아보자.

이를 위해 Selling Pricerk 33 Lakhs 이상이거나 자동차가 400,000 Km 이상 주행한 경우 이상치라고 가정하자. 그것들을 그래프에서 녹색으로 표시할 것이다. `removing_index` 변수에 모든 인덱스를 저장한다. `seavorn` 라이브러리를 사용하여 각 열을 목표 열과 비교하여 산점도 형식으로 디스플레이한다.

```python
# Outliers Labeling
df1 = df.copy()
df1['label'] = 'Normal'
df1.loc[outliers_indexes,'label'] = 'Outlier'

# Removing Outliers
removing_indexes = []
removing_indexes.extend(df1[df1[target]>33].index)
removing_indexes.extend(df1[df1['Kms_Driven']>400000].index)
df1.loc[removing_indexes,'label'] = 'Removing'

# Plot
target = 'Selling_Price'
features = df.columns.drop(target)
colors = ['#0055ff','#ff7000','#23bf00']
CustomPalette = sns.set_palette(sns.color_palette(colors))
fig, ax = plt.subplots(nrows=3 ,ncols=3, figsize=(15,12), dpi=200)

for i in range(len(features)):
    x=i//3
    y=i%3
    sns.scatterplot(data=df1, x=features[i], y=target, hue='label', ax=ax[x,y])
    ax[x,y].set_title('{} vs. {}'.format(target, features[i]), size = 15)
    ax[x,y].set_xlabel(features[i], size = 12)
    ax[x,y].set_ylabel(target, size = 12)
    ax[x,y].grid()

ax[2, 1].axis('off')
ax[2, 2].axis('off')
plt.tight_layout()
plt.show()
```

![](./images/screenShot_05.png)

완벽한 이상치를 보자.

```python
# List of original outliers indices
removing_indexes = list(set(removing_indexes))
removing_indexes
```

![](./images/screenShot_06.png)

2개 나왔다. 제거해야 하는데 그 전에 데이터세트에 null 데이터가 있는지 확인해야 한다.

```python
# Find the number of null entries in each column
df.isnull().sum()
```

데이터세트에 null 값이 없다.

![](./images/screenShot_07.png)

식별된 이상치를 제거하고 데이터 프레임의 인덱스를 재설정한다.

```python
# Remove the outliers from the dataframe
df1 = df.copy()
df1.drop(removing_indexes, inplace=True)
df1.reset_index(drop=True, inplace=True)
```

### 데이터세트 분석
데이터를 분석하여 각 필드/카테고리가 자동차 판매 가격과 상관관계가 있는지 알아보자. 데이터세트에 대한 몇 가지 결론을 얻기 위하여 데이터세트에 대한 몇 가지 분석을 해야 한다.

이를 위해 데이터세트에서 수치 필드와 범주 필드를 식별해야 한다. 이는 플롯하는 방법이 타입별로 다르기 때문이다.

```python
# List of numerical and categorical columns
NumCols = ['Age', 'Selling_Price', 'Present_Price', 'Kms_Driven', 'Owner']
CatCols = ['Fuel_Type', 'Seller_Type', 'Transmission']
```

#### 이변량 분석
이변량 분석의 [기본적인 정의](https://en.wikipedia.org/wiki/Bivariate_analysis)는 다음과 같다.

> *이변량 분석은 정량적 분석의 가장 간단한 형태 중 하나이다. 이변량 분석은 두 변수 간의 경험적 관계를 결정하기 위한 목적으로 두 변수의 분석을 포함한다. 이변량 분석은 단순한 연관성 가설을 검정하는 데 도움이 될 수 있다.*

이변량 분석을 사용하여 Selling Price을 다른 열과 비교하고 그 데이터로부터 몇 가지 결론을 도출해 보자.

##### Selling Price vs 수치 특징 이변량 분석
이변량 분석을 사용하여 수치 특징(feature)과 Selling Price을 비교해 보자. 수치 열은 산점도 그래프로 디스플레이된다.

```python
# Comparison of numerical columns with Selling price
fig, ax = plt.subplots(nrows=2 ,ncols=2, figsize=(10,10), dpi=90)
num_features = ['Present_Price', 'Kms_Driven', 'Age', 'Owner']
target = 'Selling_Price'
c = '#0055ff'

for i in range(len(num_features)):
    row = i//2
    col = i%2
    ax[row,col].scatter(df1[num_features[i]], df1[target], color=c, edgecolors='w', linewidths=0.25)
    ax[row,col].set_title('{} vs. {}'.format(target, num_features[i]), size = 12)
    ax[row,col].set_xlabel(num_features[i], size = 12)
    ax[row,col].set_ylabel(target, size = 12)
    ax[row,col].grid()

plt.suptitle('Selling Price vs. Numerical Features', size = 20)
plt.tight_layout()
plt.show()
```

![](./images/screenShot_08.png)

##### Selling Price vs 범주형 특징 이변량 분석
이변량 분석을 사용하여 범주형 특징과 Selling Price을 비교해 보자. 범주형 열은 줄무늬 그래프을 사용한다. 이를 통해 범주 내 여러 값을 비교할 수 있다.

```python
# Comparison of categorical columns with Selling price
fig, axes = plt.subplots(nrows=1 ,ncols=3, figsize=(12,5), dpi=100)
cat_features = ['Fuel_Type', 'Seller_Type', 'Transmission']
target = 'Selling_Price'
c = '#0055ff'

for i in range(len(cat_features)):
    sns.stripplot(ax=axes[i], x=cat_features[i], y=target, data=df1, size=6, color=c)
    axes[i].set_title('{} vs. {}'.format(target, cat_features[i]), size = 13)
    axes[i].set_xlabel(cat_features[i], size = 12)
    axes[i].set_ylabel(target, size = 12)
    axes[i].grid()

plt.suptitle('Selling Price vs. Categorical Features', size = 20)
plt.tight_layout()
plt.show()
```

![](./images/screenShot_09.png)

데이터 분석을 통해 얻을 수 있는 결론은 다음과 같다.

1. Present Price가 증가함에 따라 Selling Price도 증가한다. 그들은 정비례한다.
1. Selling Price은 Kilometers Driven에 반비례한다.
1. Selling Price은 자동차의 age에 반비례한다.
1. 이전 자동차 소유자의 수가 증가함에 따라 Selling Price은 낮아진다. 따라서 판매 가격은 Owner에 반비례한다.
1. Selling Price 측면에서 Diesel Cars > CNG Cars > Petrol Car.
1. 개인이 판매하는 자동차의 Selling Price는 딜러가 판매하는 자동차의 판매가격보다 낮다.
1. Automatic 자동차는 Manual 자동차보다 더 비싸다.

### 범주형 변수 인코딩
범주형 필드를 그대로 사용할 수는 없다. 기계가 숫자만 이해할 수 있기 때문에 그것들은 숫자로 변환되어야 한다.

예를 들어, Fuel 열을 살펴보자. 데이터세트에 따라 두 가지 유형의 연료를 사용하는 자동차가 있다. Petrol과 Diesel이다. 범주형 변수 인코딩은 연료 열을 두 개의 열(Fuel_Type_Petrol과 Fuel_Type_Petrol)로 분할한다.

자동차가 Petrol로 운행한다고 가정하자. 이 자동차의 경우, 데이터는 Fuel_Type_Petrol 열을 1로 설정하고 Fuel_Type_Petrol 열을 0으로 설정하는 것으로 변환된다. 컴퓨터는 "Petrol"와 "Diesel" 대신에 1과 0을 이해할 수 있다.

이를 위해 범주형 열에 대한 one-hot 인코딩을 수행할 것이다. Pandas는 열을 인코딩하는 `get_dummies` 메서드을 제공한다.

```python
# One-Hot Encode the categorical columns
CatCols = ['Fuel_Type', 'Seller_Type', 'Transmission']

df1 = pd.get_dummies(df1, columns=CatCols, drop_first=True)
df1.head(5)
```

![](./images/screenShot_10.png)

True와 False가 각각 0과 1이라고 가정한다.

### 상관관계 분석(Correlation Analysis)
상관 행렬은 데이터 집합에서 변수 쌍 간의 선형 관계의 강도와 방향을 요약하는 행렬이다. 변수 간의 연관성 패턴을 분석하고 변수 간의 연관성을 파악하는 데 사용되는 통계 및 데이터 분석의 중요한 도구이다.

값이 양이면 상관관계가 정비례하고, 음이면 반비례한다.

Selling Proce와 관련하여 상관관계 행렬을 찾는 코드는 다음과 같다.

```python
# Plot the correlation matrix
target = 'Selling_Price'
cmap = sns.diverging_palette(125, 28, s=100, l=65, sep=50, as_cmap=True)
fig, ax = plt.subplots(figsize=(9, 8), dpi=80)
ax = sns.heatmap(pd.concat([df1.drop(target,axis=1), df1[target]],axis=1).corr(), annot=True, cmap=cmap)
plt.show()
```

![](./images/screenShot_11.png)

위 행렬로부터 목표 변수 "Selling Proce"가 Present Price, Seller Type 및 Fuel Type과 높은 상관 관계가 있음을 추론할 수 있다.

### 모델 구축 방법
이제 마무리 단계에 이르렀다. 모델을 훈련하고 테스트해 보겠다.

"Selling_Price"를 입력에서 제거하고 출력으로 설정하자. 이는 예측해야 한다는 것을 의미한다.

```python
# Split the input and output (Selling Price) into separate dataframes
X = df1.drop('Selling_Price', axis=1)
y = df1['Selling_Price']
```

훈련 데이터로 70%와 테스트 데이터로 30%를 사용할 수 있도록 데이터세트를 분할한다.

```python
# Split original dataset into test and training dataset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)
```

테스트 데이터를 백업해 두자. 최종 비교를 위해서는 이것이 필요하다.

```python
# Have a copy of original output
y_test_actual = y_test
```

#### 데이터세트 정규화
`StandardScaler`는 데이터세트의 특징(변수)을 표준화하거나 정규화하기 위해 기계 학습 및 데이터 분석에서 일반적으로 사용되는 전처리 기법이다. 각 특징이 평균(평균)이 0이고 표준 편차가 1이 되도록 데이터를 변환하는 것이 주요 목적이다.

`StandardScaler`를 사용하여 데이터세트를 정규화한다.

```python
# Normalize the input dataset
scaler = StandardScaler()
scaler.fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)
```

`StandardScaler` 변환은 훈련 세트에서만 수행하여야 하며, 그렇지 않으면 데이터 유출로 이어질 것이라는 점이 매우 중요하다.

#### 모델 훈련

```python
# Train Model
linear_reg = LinearRegression()
linear_reg.fit(X_train_scaled, y_train)
```

훈련 데이터세트의 각 열에 대한 절편과 계수를 찾아보자.

```python
# Find the slope and intercept
pd.DataFrame(data = np.append(linear_reg.intercept_ , linear_reg.coef_), index = ['Intercept']+[col+" Coef." for col in X.columns], columns=['Value']).sort_values('Value', ascending=False)
```

![](./images/screenShot_12.png)

#### 모델 평가
Scikit Learn은 모델의 메트릭을 측정하는 데 도움이 되는 메트릭 기능을 제공한다. 이를 사용하여 평균 제곱 오차, 평균 절대 오차, 평균 제곱 오차 및 R2-Score를 얻을 수 있다.

이제 모델을 평가할 시간이다.

```python
# Evaluation of the model
def model_evaluation(model, X_test, y_test, model_name):
    y_pred = model.predict(X_test)
    
    MAE = metrics.mean_absolute_error(y_test, y_pred)
    MSE = metrics.mean_squared_error(y_test, y_pred)
    RMSE = np.sqrt(MSE)
    R2_Score = metrics.r2_score(y_test, y_pred)
    
    return pd.DataFrame([MAE, MSE, RMSE, R2_Score], index=['MAE', 'MSE', 'RMSE' ,'R2-Score'], columns=[model_name])

model_evaluation(linear_reg, X_test_scaled, y_test, 'Linear Reg.')
```

![](./images/screenShot_13.png)

#### K-fold 교차 검증을 사용한 모델 평가
k-fold 교차 검증에서, 데이터세트는 대략 동일한 크기의 부분집합들 또는 "folds"로 분할된다. 모델은 k번 훈련되고 평가되며, 매번 다른 폴드를 검증 세트로 사용하고 나머지 폴드들을 훈련 세트로 사용한다.

그런 다음 이러한 k번 실행의 결과(예: 정확도, 오차)를 평균하여 모델의 성능에 대한 보다 강력한 추정치를 얻는다.

각 데이터 포인트가 훈련과 검증 모두에 사용되어 평가의 편향 위험을 줄일 수 있다는 장점이 있다.

```python
# Model evaluation using K-Fold Cross-Validation
linear_reg_cv = LinearRegression()
scaler = StandardScaler()
pipeline = make_pipeline(StandardScaler(),  LinearRegression())

kf = KFold(n_splits=6, shuffle=True, random_state=0) 
scoring = ['neg_mean_absolute_error', 'neg_mean_squared_error', 'neg_root_mean_squared_error', 'r2']
result = cross_validate(pipeline, X, y, cv=kf, return_train_score=True, scoring=scoring)

MAE_mean = (-result['test_neg_mean_absolute_error']).mean()
MAE_std = (-result['test_neg_mean_absolute_error']).std()
MSE_mean = (-result['test_neg_mean_squared_error']).mean()
MSE_std = (-result['test_neg_mean_squared_error']).std()
RMSE_mean = (-result['test_neg_root_mean_squared_error']).mean()
RMSE_std = (-result['test_neg_root_mean_squared_error']).std()
R2_Score_mean = result['test_r2'].mean()
R2_Score_std = result['test_r2'].std()

pd.DataFrame({'Mean': [MAE_mean,MSE_mean,RMSE_mean,R2_Score_mean], 'Std': [MAE_std,MSE_std,RMSE_std,R2_Score_std]},
             index=['MAE', 'MSE', 'RMSE' ,'R2-Score'])
```

![](./images/screenShot_14.png)

### 결과 시각화
실제 값과 예측 값으로 데이터 프레임을 만들어 버자.

```python
# Create a dataframe with actual and predicted values
y_test_pred = linear_reg.predict(X_test_scaled)
df_comp = pd.DataFrame({'Actual':y_test_actual, 'Predicted':y_test_pred})
```

막대 그래프을 사용하여 테스트 데이터에 대한 실제 목표값과 예측 목표값을 비교해 보겠다.

```python
# Plot the Predicted vs Actual values in a graph
def compare_plot(df_comp):
    df_comp.reset_index(inplace=True)
    df_comp.plot(y=['Actual','Predicted'], kind='bar', figsize=(20,7), width=0.8)
    plt.title('Predicted vs. Actual Target Values for Test Data', fontsize=20)
    plt.ylabel('Selling_Price', fontsize=15)
    plt.show()

compare_plot(df_comp)
```

![](./images/screenShot_15.png)

위의 그래프에서 파란색 선은 실제 가격을 나타내고 주황색 선은 자동차의 예측 가격을 나타낸다. 어떤 예측 값은 음수임을 알 수 있다. 그러나 대부분의 경우에 우리 모델은 그것을 꽤 잘 예측했다. 그러나 이 모델은 완벽한 모델은 아니다.

### 맺으며
이 튜토리얼에서는 선형 회귀 분석에 대해 실용적인 예를 들어 설명하였다. ML 여정을 발전시키는 데 도움이 되기를 바한다. 위의 데이터세트와 해당 데이터세트에 대한 코드는 이 [GitHub repo](https://github.com/5minslearn/Car-Price-Prediction-using-Linear-Regression)에서 액세스할 수 있다.

<a name="footnote_1">1</a>: [How to Build a Linear Regression Model – Machine Learning Example](https://www.freecodecamp.org/news/build-a-linear-regression-model-with-an-example/)의 예를 참조하였다.