# 회귀 분석 기법 및 알고리즘 <sup>[1](#footnote_1)</sup>

> <font size="3">다양한 알고리즘을 이용하여 회귀 분석을 수행하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./regression-intro.md#intro)
1. [회귀 분석이란?](./regression-intro.md#sec_02)
1. [회귀 분석 기법의 종류](./regression-intro.md#sec_03)
1. [선형 회귀](./linear-regression.md)
1. [로지스틱 회귀](./logistic-regression.md)
1. [다항식 회귀 분석](./polynomial-regression.md)
1. [ridge 회귀 분석](./regression.md#sec_07)
1. [lasso 회귀 분석](./regression.md#sec_08)
1. [탄력 순 회귀 분석](./regression.md#sec_09)
1. [support 벡터 회귀 분석](./regression.md#sec_10)
1. [의사 결정 트리 회귀 분석](./regression.md#sec_11)
1. [랜덤 포레스트 회귀 분석](./regression.md#sec_12)
1. [그래디언트 부스팅 회귀 분석](./regression.md#sec_13)
1. [신경망 회귀 분석](./regression.md#sec_14)
1. [요약](./regression.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 3 — Exploring Regression Techniques and Algorithms](https://medium.com/ai-advances/ml-tutorial-3-exploring-regression-techniques-and-algorithms-9026b92c2487?sk=7de34d5d0d986a8f67b050301bb9ec83)를 편역한 것이다.
